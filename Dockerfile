FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > zookeeper.log'

COPY zookeeper .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' zookeeper
RUN bash ./docker.sh

RUN rm --force --recursive zookeeper
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD zookeeper
